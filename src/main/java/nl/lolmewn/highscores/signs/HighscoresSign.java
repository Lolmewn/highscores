package nl.lolmewn.highscores.signs;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.UUID;
import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 * @author Sybren
 */
public class HighscoresSign {

    private final Main plugin;
    private final Location loc;
    private final Highscore hs;

    private int current = 0;

    public HighscoresSign(Main main, Highscore hs, Location loc) {
        this.plugin = main;
        this.loc = loc;
        this.hs = hs;
    }

    public boolean isActive() {
        return loc.getWorld().isChunkLoaded(loc.getChunk());
    }

    public String[] getLines() {
        String[] lines = new String[4];
        lines[0] = "[" + ChatColor.YELLOW + "Highscore" + ChatColor.BLACK + "]";
        lines[1] = hs.getName();
        SortedSet<Map.Entry<UUID, Double>> set = hs.entriesSortedByValues();
        Iterator<Map.Entry<UUID, Double>> it = set.iterator();
        int i = 0;
        if (set.isEmpty()) {
            lines[2] = "No highscores";
            lines[3] = "for this stat";
            return lines;
        }
        if (set.size() < current) {
            current = 0;
        }
        while (it.hasNext() && i++ != current) {
            it.next(); //skip all entries we don't need
        }
        if (it.hasNext()) {
            Entry<UUID, Double> entry = it.next();
            lines[2] = (current + 1) + ") " + plugin.getServer().getOfflinePlayer(entry.getKey()).getName();
            lines[3] = Double.toString(entry.getValue());
        } else {
            current = 0;
            return this.getLines();
        }
        current = ++current % 10;
        return lines;
    }

    public Highscore getHigscore() {
        return hs;
    }

}
