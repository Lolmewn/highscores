package nl.lolmewn.highscores.signs;

import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * @author Sybren
 */
public class SignListener implements Listener {

    private final Main plugin;

    public SignListener(Main main) {
        this.plugin = main;
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        if (!event.getLine(0).equalsIgnoreCase("[Highscore]")) {
            return;
        }
        if (!event.getPlayer().hasPermission("highscores.sign.create")) {
            event.getPlayer().sendMessage("You do not have permissions to create a Highscore sign!");
            event.setCancelled(true);
            return;
        }
        String hsName = event.getLine(1);
        Highscore hs = plugin.findHighscore(event.getPlayer(), hsName);
        if(hs == null){
            event.setCancelled(true);
            return; //messages are already sent by plugin.findHighscore
        }
        plugin.getSignManager().addSign(event.getBlock(), hs);
        event.getPlayer().sendMessage("Highscores sign succesfully created!");
    }

}
