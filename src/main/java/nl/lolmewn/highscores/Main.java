package nl.lolmewn.highscores;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import nl.lolmewn.highscores.Updater.UpdateType;
import nl.lolmewn.highscores.signs.SignListener;
import nl.lolmewn.highscores.signs.SignManager;
import nl.lolmewn.highscores.workers.HighscoreSaveThread;
import nl.lolmewn.highscores.workers.ListingsSender;
import nl.lolmewn.highscores.workers.ScoresSaveThread;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Main extends JavaPlugin {

    private StatsAPI api;
    private Economy economy;
    private HighscoreManager highscoreManager;
    private SignManager signManager;
    private MySQLHelper mysqlHelper;
    private Settings settings;
    protected double newVersion;
    public final String USER_TABLE = "Highscores_players";
    public final String HIGHSCORE_TABLE = "Highscores_highscores";

    @Override
    public void onEnable() {
        Plugin stats = this.getServer().getPluginManager().getPlugin("Stats");
        if (stats == null) {
            this.getLogger().severe("Stats not found, disabling! You can download stats here: http://dev.bukkit.org/server-mods/lolmewnstats/");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!stats.isEnabled()) {
            this.getLogger().severe("Stats plugin has been disabled, Achievements cannot start!");
            this.getLogger().severe("Please resolve any Stats issues first!");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        api = getServer().getServicesManager().getRegistration(nl.lolmewn.stats.api.StatsAPI.class).getProvider();
        this.getConfig().options().copyDefaults();
        this.saveConfig();
        this.settings = new Settings(this);
        this.settings.loadSettings();
        try {
            this.prepareMySQL();
            this.highscoreManager = new HighscoreManager(this);
            this.highscoreManager.loadHighscores();
            this.highscoreManager.loadScoresForHighscores();
            this.getLogger().info("Loaded " + this.highscoreManager.getHighscores().size() + " highscores.");

            this.signManager = new SignManager(this);
            this.signManager.loadSigns();
        } catch (SQLException ex) {
            this.getLogger().severe("Something failed with MySQL! Are the details correct, is it up?");
            this.getLogger().severe("In any case, I'm disabling myself.");
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new ScoresSaveThread(this), 600L, 600L);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new HighscoreSaveThread(this), 20L, 660L);
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        this.getServer().getPluginManager().registerEvents(new SignListener(this), this);
        if (this.getSettings().isUpdate()) {
            new Updater(this, 57740, this.getFile(), UpdateType.DEFAULT, false);
        }
        try {
            Metrics m = new Metrics(this);
            m.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void onDisable() {
        this.getServer().getScheduler().cancelTasks(this);
        new ScoresSaveThread(this).run();
        this.getSignManager().saveSigns();
    }

    public Settings getSettings() {
        return settings;
    }

    public StatsAPI getStatsAPI() {
        return api;
    }

    public HighscoreManager getHighscoreManager() {
        return this.highscoreManager;
    }

    public SignManager getSignManager() {
        return this.signManager;
    }

    public MySQLHelper getMysqlHelper() {
        return mysqlHelper;
    }

    private void prepareMySQL() throws SQLException {
        this.mysqlHelper = new MySQLHelper();
        Connection con = this.getStatsAPI().getConnection();
        if (!mysqlHelper.tableExists(con, "Highscores_players")) {
            Statement create = con.createStatement();
            create.execute("CREATE TABLE " + this.USER_TABLE
                    + "(counter INT PRIMARY KEY NOT NULL AUTO_INCREMENT,"
                    + "player_id INT NOT NULL,"
                    + "highscoreId INT NOT NULL,"
                    + "value BIGINT SIGNED NOT NULL,"
                    + "isHighscore BOOLEAN DEFAULT FALSE NOT NULL)");
            create.close();
        } else {
            mysqlHelper.convertFormat(api, con, this.USER_TABLE);
        }
        if (!mysqlHelper.tableExists(con, "Highscores_highscores")) {
            Statement create = con.createStatement();
            //TODO put dem highscores in here.
            create.execute("CREATE TABLE " + this.HIGHSCORE_TABLE
                    + "(counter INT PRIMARY KEY NOT NULL," //no auto_increment because it's set in the config
                    + "name varchar(255) NOT NULL,"
                    + "description TEXT,"
                    + "goals TEXT NOT NULL,"
                    + "rewards TEXT,"
                    + "highscorePlayer varchar(255),"
                    + "highscoreValue BIGINT SIGNED)");
            create.close();
        }
        con.close();
    }

    public void debug(String string) {
        if (this.getSettings().isDebug()) {
            this.getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "[Debug]" + ChatColor.RESET + string);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            showHighscores(sender, 1);
            return true;
        }
        if (args[0].equalsIgnoreCase("page")) {
            if (args.length == 1) {
                sender.sendMessage(ChatColor.GREEN + "View next pages by performing " + ChatColor.RED + "/" + label + " page <page>");
                return true;
            }
            try {
                int page = Integer.parseInt(args[1]);
                if (page <= 0) {
                    sender.sendMessage(ChatColor.RED + "Pagenumber can only be higher than 0!");
                    return true;
                }
                this.showHighscores(sender, page);
                return true;
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Page can only be a number!");
                return true;
            }
        }
        if(args[0].equalsIgnoreCase("me")){
            if(!(sender instanceof Player)){
                sender.sendMessage("Only players can perform this command!");
                return true;
            }
            int page;
            if(args.length == 1){
                page = 1;
            }else{
                try{
                    page = Integer.parseInt(args[1]);
                }catch(NumberFormatException e){
                    sender.sendMessage("Correct usage: /hs me [page]");
                    return true;
                }
            }
            new ListingsSender(sender, ((Player)sender).getUniqueId(), page, this);
            return true;
        }
        if (args[0].equalsIgnoreCase("help")) {
            sender.sendMessage(ChatColor.GREEN + "=====Highscores by " + ChatColor.RED + "Lolmewn" + ChatColor.GREEN + "=====");
            sender.sendMessage("View all highscores    : " + ChatColor.RED + "/" + label + "");
            sender.sendMessage("View highscores on page: " + ChatColor.RED + "/" + label + " page <page>");
            sender.sendMessage("View top highscores    : " + ChatColor.RED + "/" + label + " top <highscore>");
            return true;
        }
        if (args[0].equalsIgnoreCase("top")) {
            if (args.length == 1) {
                sender.sendMessage(ChatColor.GREEN + "View a highscores' top players by performing " + ChatColor.RED + "/" + label + " top <highscoreName>");
                return true;
            }
            String key = args[1];
            Highscore high = this.findHighscore(sender, key);
            if (high != null) {
                sender.sendMessage(ChatColor.GREEN + "===Highscore " + ChatColor.RED + high.getName() + ChatColor.GREEN + " top rankings===");
                SortedSet<Map.Entry<UUID, Double>> set = high.entriesSortedByValues();
                Iterator<Entry<UUID, Double>> it = set.iterator();
                for (int i = 0; i < 10; i++) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Entry<UUID, Double> entry = it.next();
                    sender.sendMessage("" + ChatColor.RED + (i + 1) + ": " + ChatColor.GREEN + entry.getValue() + ChatColor.RED + " by " + ChatColor.GREEN + 
                            this.getServer().getOfflinePlayer(entry.getKey()).getName());
                }
            }
            return true;
        }
        return false;
    }

    private void showHighscores(CommandSender sender, int page) {
        Collection<Highscore> scores = this.getHighscoreManager().getHighscores();
        int pp = this.getSettings().getItemsPerPage();
        int shown = 0;
        int skipped = 0;
        List<Integer> hs_shown = new ArrayList<Integer>();
        if ((page - 1) * pp >= scores.size()) {
            sender.sendMessage(ChatColor.RED + "There's no highscores page " + page + "!");
            return;
        }
        sender.sendMessage(ChatColor.GREEN + "=====Highscores page "
                + ChatColor.RED + page + ChatColor.GREEN + "/"
                + ChatColor.BLUE
                + (scores.size() % pp == 0 ? scores.size() / pp : scores.size() / pp + 1)
                + ChatColor.GREEN + "=====");
        for (Stat type : this.getSettings().getOrdering()) {
            List<Highscore> hs = this.getHighscoreManager().getHighscoresForStat(type);
            if (hs == null || hs.isEmpty()) {
                continue;
            }
            for (Highscore score : hs) {
                if (hs_shown.contains(score.getId())) {
                    continue;
                }
                if (skipped < pp * (page - 1)) {
                    skipped++;
                    continue;
                }
                if (shown == pp) {
                    return;
                }
                sender.sendMessage(ChatColor.GREEN + score.getName() + ": "
                        + ChatColor.RED + score.getCurrentHighscoreValue()
                        + ChatColor.GREEN + " by "
                        + ChatColor.RED + score.getCurrentHighscorePlayer());
                shown++;
                hs_shown.add(score.getId());
            }
        }
    }

    public Highscore findHighscore(CommandSender sender, String key) {
        try {
            int id = Integer.parseInt(key);
            Highscore high = this.getHighscoreManager().getHighscore(id);
            if (high == null && sender != null) {
                sender.sendMessage(ChatColor.RED + "Highscore with ID " + key + " not found!");
            }
            return high;
        } catch (NumberFormatException e) {
            for (Highscore score : this.getHighscoreManager().getHighscores()) {
                if (score.getName().replace(" ", "").equalsIgnoreCase(key)) {
                    return score;
                }
            }
            for (Highscore score : this.getHighscoreManager().getHighscores()) {
                if (score.getName().toLowerCase().replace(" ", "").startsWith(key.toLowerCase())) {
                    return score;
                }
            }
            if (sender != null) {
                sender.sendMessage(ChatColor.RED + "Highscore with name " + key + " not found!");
            }
            return null;
        }
    }    

    public boolean setupEconomy() {
        if (economy != null) {
            return true;
        }
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    public Economy getEconomy() {
        return this.economy;
    }
}
