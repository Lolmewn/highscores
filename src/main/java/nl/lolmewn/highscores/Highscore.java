/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import nl.lolmewn.highscores.completion.Completion;
import nl.lolmewn.highscores.completion.CompletionType;
import nl.lolmewn.highscores.goal.Goal;
import nl.lolmewn.highscores.reward.Reward;
import nl.lolmewn.highscores.reward.RewardType;
import nl.lolmewn.stats.api.Stat;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Highscore {

    private final Main main;
    private final int id;
    private String name, desc;
    private final List<Goal> goals = new ArrayList<Goal>();
    private final List<Reward> rewards = new ArrayList<Reward>();
    private final List<Completion> comp = new ArrayList<Completion>();
    private final List<Stat> types = new ArrayList<Stat>();
    private double databaseValue;
    private double currentHighscoreValue;
    private UUID currentHighscorePlayerUUID;
    private final ConcurrentHashMap<UUID, Double> listings = new ConcurrentHashMap<UUID, Double>();
    private final Set<UUID> hasUpdate = Collections.synchronizedSet(new HashSet<UUID>());
    private final HashMap<UUID, Double> dbListings = new HashMap<UUID, Double>();

    public Highscore(Main m, int id, String name, String description, List<Goal> goals, List<Reward> rewards, List<Completion> comp, final Stat type) {
        this.main = m;
        this.id = id;
        this.name = name;
        this.desc = description;
        if (goals != null) {
            this.goals.addAll(goals);
        }
        if (rewards != null) {
            this.rewards.addAll(rewards);
        }
        if (comp != null) {
            this.comp.addAll(comp);
        }
        this.types.add(type);
    }

    public Highscore(Main m, int id, ConfigurationSection load) {
        this.main = m;
        this.id = id;
        this.load(load);
    }

    public final boolean load(ConfigurationSection loadFrom) {
        name = loadFrom.getString("name");
        desc = loadFrom.getString("description", "The " + name + " achievement.");
        for (String goal : loadFrom.getStringList("goals")) {
            String[] split = goal.split(" ");
            if (split.length < 2) {
                main.getLogger().warning("Unable to load achievement " + name + ", goal is set up wrong: " + goal);
                return false;
            }
            Stat stat;
            try {
                stat = main.getStatsAPI().getStat(split[0].replace("_", " "));
            } catch (Exception e) {
                main.getLogger().warning("Unable to load highscore " + name + ", stat was not found: " + split[0]);
                return false;
            }
            if (stat == null) {
                main.getLogger().warning("Unable to load highscore " + name + ", stat was not found: " + split[0]);
                return false;
            }
            this.types.add(stat);
            Goal g;
            if (split.length != 3 && split[1].equalsIgnoreCase("TOTAL")) {
                g = new Goal(stat, true, null);
            } else {
                if (split.length == 2) {
                    g = new Goal(stat, false, new Object[]{});
                } else {
                    Object[] vars = new Object[split.length - 2];
                    System.arraycopy(split, 2, vars, 0, vars.length);
                    g = new Goal(stat, false, vars);
                }
            }
            this.goals.add(g);
            main.debug("Goal created: " + g.toString());
        }
        if (this.goals.isEmpty()) {
            main.getLogger().warning("Unable to load highscore " + name + ", no goals specified");
            return false;
        }
        if (loadFrom.contains("rewards")) {
            loadRewards(loadFrom.getConfigurationSection("rewards"));
        }
        if (loadFrom.contains("onGet")) {
            loadOnGet(loadFrom.getConfigurationSection("onGet"));
        }
        return true;
    }

    public List<Completion> getComp() {
        return comp;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public List<Reward> getRewards() {
        return rewards;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return desc;
    }

    private void loadRewards(ConfigurationSection loadFrom) {
        if (loadFrom.contains("money")) {
            this.rewards.add(new Reward(RewardType.MONEY, loadFrom.getInt("money")));
        }
        if (loadFrom.contains("items")) {
            String items = loadFrom.getString("items");
            if (items.contains(";")) {
                for (String item : items.split(";")) {
                    if (!item.contains(",")) {
                        main.getLogger().warning("Unable to load item for highscore " + name + ", no amount set");
                    } else {
                        try {
                            Integer.parseInt(item.split(",")[1]);
                            this.rewards.add(new Reward(RewardType.ITEM, item));
                        } catch (NumberFormatException e) {
                            main.getLogger().warning("Unable to load item for highscore " + name + ", amount is no number");
                        }
                    }
                }
            }
        }
        if (loadFrom.contains("commands")) {
            for (String command : loadFrom.getStringList("commands")) {
                this.rewards.add(new Reward(RewardType.COMMAND, command));
            }
        }
        if (loadFrom.contains("consoleCommands")) {
            for (String command : loadFrom.getStringList("consoleCommands")) {
                this.rewards.add(new Reward(RewardType.CONSOLE_COMMAND, command));
            }
        }
    }

    private void loadOnGet(ConfigurationSection loadFrom) {
        if (loadFrom.contains("messages")) {
            for (String message : loadFrom.getStringList("messages")) {
                this.comp.add(new Completion(CompletionType.MESSAGE, message));
            }
        }
        if (loadFrom.contains("broadcast")) {
            for (String message : loadFrom.getStringList("broadcast")) {
                this.comp.add(new Completion(CompletionType.BROADCAST, message));
            }
        }
    }

    public int getId() {
        return this.id;
    }

    public double getCurrentHighscoreValue() {
        if (this.currentHighscorePlayerUUID != null) {
            return this.currentHighscoreValue;
        }
        return 0;
    }

    public void setCurrentHighscoreValue(double currentHighscoreValue) {
        this.currentHighscoreValue = currentHighscoreValue;
    }

    public String getCurrentHighscorePlayer() {
        if(this.currentHighscorePlayerUUID == null){
            return "No-one";
        }
        OfflinePlayer op = this.main.getServer().getOfflinePlayer(currentHighscorePlayerUUID);
        if(!op.hasPlayedBefore()){
            return "No-one";
        }
        return op.getName();
    }
    
    public UUID getCurrentHighscorePlayerUUID(){
        return this.currentHighscorePlayerUUID;
    }

    public void setCurrentHighscorePlayer(String currentHighscorePlayer) {
        OfflinePlayer op = this.main.getServer().getOfflinePlayer(currentHighscorePlayer);
        this.currentHighscorePlayerUUID = op.getUniqueId();
    }
    
    public void setCurrentHighscorePlayer(UUID uuid){
        this.currentHighscorePlayerUUID = uuid;
    }

    public double getDatabaseValue() {
        return databaseValue;
    }

    public void setDatabaseValue(long databaseValue) {
        this.databaseValue = databaseValue;
    }

    public List<Stat> getStatTypes() {
        return this.types;
    }

    public <UUID, Double extends Comparable<? super Double>> SortedSet<Map.Entry<UUID, Double>> entriesSortedByValues() {
        Map<UUID, Double> map = (Map<UUID, Double>) this.listings;
        SortedSet<Map.Entry<UUID, Double>> sortedEntries = new TreeSet<Map.Entry<UUID, Double>>(
                new Comparator<Map.Entry<UUID, Double>>() {

                    @Override
                    public int compare(Map.Entry<UUID, Double> e1, Map.Entry<UUID, Double> e2) {
                        int res = e2.getValue().compareTo(e1.getValue());
                        if (e1.getKey().equals(e2.getKey())) {
                            return res; // Code will now handle equality properly
                        } else {
                            return res != 0 ? res : 1; // While still adding all entries
                        }
                    }
                });
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    public void addListing(String user, double value, boolean shouldBeUpdatedToDatabase) {
        OfflinePlayer op = this.main.getServer().getOfflinePlayer(user);
        this.listings.put(op.getUniqueId(), value);
        if (shouldBeUpdatedToDatabase) {
            this.hasUpdate.add(op.getUniqueId());
        }
    }

    public void addListing(UUID uuid, double value, boolean shouldBeUpdatedToDatabase) {
        this.listings.put(uuid, value);
        if (shouldBeUpdatedToDatabase) {
            this.hasUpdate.add(uuid);
        }
    }

    public boolean hasListing(String user) {
        return listings.containsKey(this.main.getServer().getOfflinePlayer(user).getUniqueId());
    }

    public boolean hasListing(UUID uuid) {
        return listings.containsKey(uuid);
    }

    public void updateListing(String user, double updateValue) {
        OfflinePlayer op = this.main.getServer().getOfflinePlayer(user);
        this.listings.put(op.getUniqueId(), this.listings.get(op.getUniqueId()) + updateValue);
        this.hasUpdate.add(op.getUniqueId());
    }

    public void updateListing(UUID uuid, double update) {
        this.listings.put(uuid, this.listings.get(uuid) + update);
    }

    public double getListing(String user, boolean updatingDatabase) {
        OfflinePlayer op = this.main.getServer().getOfflinePlayer(user);
        double value = this.listings.get(op.getUniqueId());
        if (updatingDatabase) {
            this.dbListings.put(op.getUniqueId(), value);
        }
        return value;
    }

    public double getListing(UUID user, boolean updatingDatabase) {
        double value = this.listings.get(user);
        if (updatingDatabase) {
            this.dbListings.put(user, value);
        }
        return value;
    }

    public HashMap<UUID, Double> getDbListings() {
        return dbListings;
    }

    public void setDbListing(UUID player, double value) {
        this.dbListings.put(player, value);
    }

    public Set<UUID> getUsersWithUpdate() {
        return this.hasUpdate;
    }
}
