/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.highscores.reward;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public enum RewardType {
    
    MONEY,
    ITEM,
    CONSOLE_COMMAND,
    COMMAND;

}
