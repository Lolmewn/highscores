package nl.lolmewn.highscores.signs;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;

/**
 * @author Sybren
 */
public class SignManager extends ConcurrentHashMap<Location, HighscoresSign> implements Listener {

    private final Main plugin;
    private final HashSet<String> cantLoad = new HashSet<String>();
    private final YamlConfiguration config;

    public SignManager(Main plugin) {
        this.plugin = plugin;
        this.scheduleUpdater();
        this.config = YamlConfiguration.loadConfiguration(this.getSignsFile());
    }

    public void loadSigns() {
        for(String key : config.getConfigurationSection("").getKeys(false)){
            int highscoreId = config.getInt(key);
            if(plugin.getHighscoreManager().getHighscore(highscoreId) == null){
                plugin.getLogger().warning("Found a sign for a highscore that doesn't exist (" + highscoreId + ") at " + key + ", removing it");
                config.set(key, null);
                continue;
            }
            String[] ar = key.split(",");
            String worldName = ar[0];
            World world = plugin.getServer().getWorld(worldName);
            if(world == null){
                plugin.getLogger().info("Found a sign in world " + worldName + ", but that world is not loaded. Waiting for that world to load.");
                this.cantLoad.add(worldName);
                continue;
            }
            int x = Integer.parseInt(ar[1]), y = Integer.parseInt(ar[2]), z = Integer.parseInt(ar[3]);
            Location loc = new Location(world, x, y, z);
            this.put(loc, new HighscoresSign(this.plugin, this.plugin.getHighscoreManager().getHighscore(highscoreId), loc));
        }
        try {
            config.save(this.getSignsFile());
        } catch (IOException ex) {
            Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveSigns() {
        for(Location loc : this.keySet()){
            HighscoresSign sign = this.get(loc);
            config.set(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ(), sign.getHigscore().getId());
        }
        try {
            config.save(this.getSignsFile());
        } catch (IOException ex) {
            Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public final File getSignsFile(){
        File file = new File(plugin.getDataFolder(), "signs.yml");
        if(!file.exists()){
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return file;
    }

    public void addSign(Block block, Highscore hs) {
        this.put(block.getLocation(), new HighscoresSign(this.plugin, hs, block.getLocation()));
        this.saveSigns();
    }

    private void scheduleUpdater() {
        this.plugin.getServer().getScheduler().runTaskTimerAsynchronously(this.plugin, new Runnable() {

            @Override
            public void run() {
                final HashMap<Location, String[]> updates = new HashMap<Location, String[]>();
                for (Location loc : keySet()) {
                    HighscoresSign sign = get(loc);
                    if(!sign.isActive()){
                        continue;
                    }
                    String[] update = sign.getLines(); //takes some time
                    updates.put(loc, update);
                }
                plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

                    @Override
                    public void run() {
                        for(Location loc : updates.keySet()){
                            Sign sign = (Sign)loc.getBlock().getState();
                            String[] lines = updates.get(loc);
                            for(int i = 0; i < lines.length && i < 4; i++){
                                sign.setLine(i, lines[i]);
                            }
                            sign.update();
                        }
                    }
                });
            }
        },
                this.plugin.getConfig().getInt("signs.update-interval", 5) * 20,
                this.plugin.getConfig().getInt("signs.update-interval", 5) * 20);
    }

}
