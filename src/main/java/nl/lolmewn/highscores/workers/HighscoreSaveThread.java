package nl.lolmewn.highscores.workers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;
import org.json.simple.JSONArray;

/**
 *
 * @author Lolmewn
 */
public class HighscoreSaveThread implements Runnable {

    private final Main plugin;

    public HighscoreSaveThread(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        Connection con = plugin.getStatsAPI().getConnection();
        try {
            PreparedStatement update = con.prepareStatement("UPDATE " + plugin.HIGHSCORE_TABLE + " SET name=?, description=?, goals=?, rewards=?, highscorePlayer=?, highscoreValue=? WHERE counter=?");
            PreparedStatement insert = con.prepareStatement("INSERT INTO " + plugin.HIGHSCORE_TABLE + " VALUES (?, ?, ?, ?, ?, ?, ?)");

            for (Highscore hs : plugin.getHighscoreManager().getHighscores()) {
                update.setString(1, hs.getName());
                update.setString(2, hs.getDescription());
                update.setString(3, JSONArray.toJSONString(hs.getGoals()));
                update.setString(4, JSONArray.toJSONString(hs.getRewards()));
                update.setInt(5, hs.getCurrentHighscorePlayerUUID() == null ? -1 : plugin.getStatsAPI().getPlayerId(hs.getCurrentHighscorePlayerUUID()));
                update.setDouble(6, hs.getCurrentHighscoreValue());
                update.setInt(7, hs.getId());
                if (update.executeUpdate() == 0) {
                    insert.setInt(1, hs.getId());
                    insert.setString(2, hs.getName());
                    insert.setString(3, hs.getDescription());
                    insert.setString(4, JSONArray.toJSONString(hs.getGoals()));
                    insert.setString(5, JSONArray.toJSONString(hs.getRewards()));
                    insert.setInt(6, hs.getCurrentHighscorePlayerUUID() == null ? -1 : plugin.getStatsAPI().getPlayerId(hs.getCurrentHighscorePlayerUUID()));
                    insert.setDouble(7, hs.getCurrentHighscoreValue());
                    insert.execute();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(HighscoreSaveThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(HighscoreSaveThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
