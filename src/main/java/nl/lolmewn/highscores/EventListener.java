/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores;

import java.util.List;
import nl.lolmewn.highscores.completion.Completion;
import nl.lolmewn.highscores.goal.Goal;
import nl.lolmewn.highscores.reward.Reward;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatUpdateEvent;
import nl.lolmewn.stats.api.StatsPlayerLoadedEvent;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class EventListener implements Listener {

    private final Main plugin;

    public EventListener(Main m) {
        this.plugin = m;
    }

    @EventHandler()
    public void login(StatsPlayerLoadedEvent event) {
        for (Highscore high : plugin.getHighscoreManager().getHighscores()) {
            if (!high.hasListing(event.getStatsPlayer().getPlayername()) || high.getListing(event.getStatsPlayer().getPlayername(), false) == 0) {
                this.loadScoreForHighscore(high, event.getStatsPlayer(), 0);
            }
        }
    }

    @EventHandler()
    public void statUpdate(StatUpdateEvent event) {
        StatsPlayer sp = event.getPlayer();
        String pName = sp.getPlayername();
        OfflinePlayer op = plugin.getServer().getOfflinePlayer(pName);
        List<Highscore> highscores = plugin.getHighscoreManager().getHighscoresForStat(event.getStat());
        if (highscores == null) {
            return;//There's no highscores for this stat.
        }
        for (Highscore high : highscores) {
            loadScoreForHighscore(high, sp, event.getUpdateValue());
        }
    }

    private void handleHighscoreGet(StatsPlayer sp, final Highscore high) {
        final String pName = sp.getPlayername();
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
            @Override
            public void run() {
                boolean invFullMessage = false;
                Player player = plugin.getServer().getPlayerExact(pName);
                for (Reward reward : high.getRewards()) {
                    switch (reward.getRewardType()) {
                        case COMMAND:
                            if (player != null) {
                                plugin.getServer().dispatchCommand(plugin.getServer().getPlayerExact(pName),
                                        reward.getStringValue().replace("%player%", pName).replace("%name%", high.getName()));
                            }
                            break;
                        case CONSOLE_COMMAND:
                            plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(),
                                    reward.getStringValue().replace("%player%", pName).replace("%name%", high.getName()));
                            break;
                        case MONEY:
                            if (plugin.setupEconomy()) {
                                plugin.getEconomy().depositPlayer(pName, reward.getIntValue());
                            }
                            break;
                        case ITEM:
                            if (player == null) {
                                break;
                            }
                            ItemStack stack = reward.getItemReward();
                            if (!player.getInventory().addItem(stack).isEmpty()) {
                                if (stack.getTypeId() == 0) {
                                    continue;
                                }
                                player.getWorld().dropItem(player.getLocation(), stack);
                                if (!invFullMessage) {
                                    player.sendMessage(ChatColor.GREEN + "Inventory full, item dropped on the ground.");
                                    invFullMessage = true;
                                }
                            }
                    }
                }
                for (Completion c : high.getComp()) {
                    switch (c.getType()) {
                        case MESSAGE:
                            Player p = plugin.getServer().getPlayerExact(pName);
                            if (p != null) {
                                p.sendMessage(ChatColor.translateAlternateColorCodes('&', c.getValue().replace("%player%", pName).replace("%name%", high.getName())));
                            }
                            break;
                        case BROADCAST:
                            plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', c.getValue().replace("%player%", pName).replace("%name%", high.getName())));
                    }
                }
            }
        });

    }

    private void loadScoreForHighscore(Highscore high, StatsPlayer sp, double updateValue) {
        String pName = sp.getPlayername();
        OfflinePlayer op = plugin.getServer().getOfflinePlayer(pName);
        if (high.getStatTypes().size() == 1) {
            Stat type = high.getStatTypes().get(0);
            double value = 0;
            for (String world : sp.getWorlds()) {
                StatData stat = sp.getStatData(type, world, false);
                if (stat == null) {
                    continue;
                }
                if (high.getGoals().get(0).isGlobal()) {
                    for (Object[] av : stat.getAllVariables()) {
                        value += stat.getValue(av);
                    }
                } else {
                    value = stat.getValue(high.getGoals().get(0).getVariables()) + updateValue;
                }
            }
            high.addListing(op.getUniqueId(), value, true);
            if (high.getListing(op.getUniqueId(), false) > high.getCurrentHighscoreValue() || high.getCurrentHighscorePlayerUUID() == null) {
                high.setCurrentHighscoreValue(value);
                if (high.getCurrentHighscorePlayerUUID() != null && !high.getCurrentHighscorePlayerUUID().equals(op.getUniqueId())) {
                    high.setCurrentHighscorePlayer(op.getUniqueId());
                    handleHighscoreGet(sp, high);
                }
            }
        } else {
            double value = 0;
            for (Goal g : high.getGoals()) {
                StatData stat = sp.getGlobalStatData(g.getStat());
                if (stat == null) {
                    continue;
                }
                if (g.isGlobal()) {
                    for (Object[] vars : stat.getAllVariables()) {
                        value += stat.getValue(vars);
                    }
                } else {
                    value += stat.getValue(g.getVariables()) + updateValue;
                }
            }
            high.addListing(op.getUniqueId(), value, true);
            if (high.getListing(op.getUniqueId(), false) > high.getCurrentHighscoreValue() || high.getCurrentHighscorePlayerUUID() == null) {
                high.setCurrentHighscoreValue(value);
                if (high.getCurrentHighscorePlayerUUID() != null && !high.getCurrentHighscorePlayerUUID().equals(op.getUniqueId())) {
                    high.setCurrentHighscorePlayer(op.getUniqueId());
                    handleHighscoreGet(sp, high);
                }
            }
        }
    }
}
