/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.highscores.goal;

import java.util.Arrays;
import nl.lolmewn.stats.api.Stat;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Goal {

    private final Stat stat;
    private final boolean global;
    private final Object[] variables;
    
    public Goal(Stat type, boolean global, Object[] variables) {
        this.stat = type;
        this.global = global;
        this.variables = variables;
    }

    public boolean isGlobal() {
        return global;
    }
    
    public Stat getStat(){
        return stat;
    }

    public Stat getType() {
        return getStat();
    }

    public Object[] getVariables() {
        return variables;
    }
    
    @Override
    public String toString(){
        return stat.toString() + ", " + global + ", " + Arrays.toString(variables);
    }

}
