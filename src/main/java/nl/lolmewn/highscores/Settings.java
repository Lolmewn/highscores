/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.api.Stat;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Settings {

    private final Main main;
    private boolean allStatsDefaultHighscores;
    private int defaultMoneyReward;
    private ItemStack[] defaultItemRewards = new ItemStack[0];
    private int itemsPerPage;
    private boolean trackTime;
    private String version;
    private boolean update;
    private boolean debug;
    private final List<Stat> ordering = new ArrayList<Stat>();

    public Settings(Main main) {
        this.main = main;
    }

    public void loadSettings() {
        main.saveDefaultConfig();
        FileConfiguration c = main.getConfig();
        c.options().copyDefaults(true);
        try {
            c.save(new File(main.getDataFolder(), "config.yml"));
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.allStatsDefaultHighscores = c.getBoolean("allStatTypesDefaultHighscores", true);
        this.defaultMoneyReward = c.getInt("defaultRewards.money", 0);
        this.itemsPerPage = c.getInt("showPerPage", 10);
        String items = c.getString("defaultRewards.items", "");
        if (!items.equals("")) {
            if (items.contains(";")) {
                String[] split = items.split(";");
                this.defaultItemRewards = new ItemStack[split.length];
                for (int i = 0; i < split.length; i++) {
                    String item = split[i];
                    if (!item.contains(",")) {
                        main.getLogger().warning("Unable to load default item '" + item + "', no amount set");
                    } else {
                        try {
                            int itemId = Integer.parseInt(item.split(",")[0]);
                            int amount = Integer.parseInt(item.split(",")[1]);
                            byte itemData = item.split(",").length == 3 ? Byte.parseByte(item.split(",")[2]) : 0;
                            this.defaultItemRewards[i] = new ItemStack(itemId, amount, itemData);
                        } catch (NumberFormatException e) {
                            main.getLogger().warning("Unable to load default item '" + item + "', amount is no number");
                        }
                    }
                }
            } else {
                this.defaultItemRewards = new ItemStack[1];
                if (!items.contains(",")) {
                    main.getLogger().warning("Unable to load default item '" + items + "', no amount set");
                } else {
                    try {
                        int itemId = Integer.parseInt(items.split(",")[0]);
                        int amount = Integer.parseInt(items.split(",")[1]);
                        byte itemData = items.split(",").length == 3 ? Byte.parseByte(items.split(",")[2]) : 0;
                        this.defaultItemRewards[0] = new ItemStack(itemId, amount, itemData);
                    } catch (NumberFormatException e) {
                        main.getLogger().warning("Unable to load default item '" + items + "', amount is no number");
                    }
                }
            }
        }

        this.trackTime = c.getBoolean("stats.trackTime", false);
        List<String> order = c.getStringList("ordering");
        for (String s : order) {
            if (s.equalsIgnoreCase("%rest%")) {
                Collection<Stat> stats = main.getStatsAPI().getAllStats();
                for (Stat stat : stats) {
                    if (this.ordering.contains(stat)) {
                        continue;
                    }
                    this.ordering.add(stat);
                }
                break;
            }
            try {
                Stat type = main.getStatsAPI().getStat(s);
                this.ordering.add(type);
            } catch (Exception e) {
                Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, s, e);
            }
        }
        this.version = c.getString("version");
        this.update = c.getBoolean("update", true);
        this.debug = c.getBoolean("debug", false);
    }

    public boolean isAllStatsDefaultHighscores() {
        return allStatsDefaultHighscores;
    }

    public boolean isDebug() {
        return debug;
    }

    public ItemStack[] getDefaultItemRewards() {
        return defaultItemRewards;
    }

    public int getDefaultMoneyReward() {
        return defaultMoneyReward;
    }

    public boolean isTrackTime() {
        return trackTime;
    }

    public boolean isUpdate() {
        return update;
    }

    public Double getVersion() {
        return version.contains("-") ? Double.parseDouble(version.split("-")[0]) : Double.parseDouble(version);
    }

    public String getVersionString() {
        return version;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public List<Stat> getOrdering() {
        return ordering;
    }
}
