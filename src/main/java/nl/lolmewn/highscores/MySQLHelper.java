/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class MySQLHelper {

    public boolean tableExists(Connection con, String table) throws SQLException {
        Statement st = con.createStatement();
        ResultSet set = st.executeQuery("SHOW TABLES LIKE '" + table + "'");
        if (set != null && !set.next()) {
            set.close();
            st.close();
            return false;
        }
        if (set != null) {
            set.close();
            st.close();
        }
        return true;
    }

    public void convertFormat(StatsAPI api, Connection con, String table) throws SQLException {
        if (hasColumn(con, table, "player")) {
            Statement st = con.createStatement();
            if (!hasColumn(con, table, "player_id")) {
                st.executeUpdate("ALTER TABLE " + table + " ADD COLUMN player_id INT AFTER counter");
            }
            st.executeUpdate("UPDATE " + table + " SET player_id=(SELECT player_id FROM " + api.getDatabasePrefix() + "players WHERE " + api.getDatabasePrefix() + "players.name=" + table + ".player)");
            st.executeUpdate("ALTER TABLE " + table + " DROP COLUMN player");
        }
    }

    public boolean hasColumn(Connection con, String table, String column) throws SQLException {
        Statement st = con.createStatement();
        ResultSet set = st.executeQuery("SELECT * FROM " + table + " LIMIT 1");
        ResultSetMetaData rsmd = set.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            if (rsmd.getColumnName(i).equals(column)) {
                return true;
            }
        }
        return false;
    }

    public int getAchievementID(Connection con, Stat stat, String table) throws SQLException {
        PreparedStatement pst = con.prepareStatement("SELECT counter FROM " + table + " WHERE name=?");
        pst.setString(1, stat.getName());
        ResultSet set = pst.executeQuery();
        if (!set.next()) {
            pst.close();
            return -1;
        }
        int val = set.getInt(1);
        pst.close();
        return val;
    }

    public int getNextFreeID(Connection con, String table) throws SQLException {
        Statement st = con.createStatement();
        ResultSet re = st.executeQuery("SELECT MAX(counter) FROM " + table);
        if(!re.next()){
            st.close();
            return -1;
        }
        int value = re.getInt(1);
        st.close();
        return value < 9000 ? -1 : value + 1;
    }

}
