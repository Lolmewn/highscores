/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores.reward;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Reward {

    private final RewardType type;
    private String stringValue;
    private int intValue;
    private ItemStack stack;

    public Reward(RewardType type, ItemStack stack) {
        this.type = type;
        this.stack = stack;
    }

    public Reward(RewardType type, int value) {
        this.type = type;
        this.intValue = value;
    }

    public Reward(RewardType type, String value) {
        this.type = type;
        this.stringValue = value;
    }

    public int getIntValue() {
        return intValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public RewardType getRewardType() {
        return type;
    }

    public ItemStack getItemReward() {
        if (stack == null) {
            String item = stringValue.split(",")[0];
            int amount = Integer.parseInt(stringValue.split(",")[1]);
            ItemStack itemStack;
            if (item.contains(".")) {
                itemStack = new ItemStack(Material.getMaterial(Integer.parseInt(item.split("\\.")[0])), amount, Short.parseShort(item.split("\\.")[1]));
            } else {
                itemStack = new ItemStack(Material.getMaterial(Integer.parseInt(item)), amount);
            }
            return itemStack;
        }
        return stack;
    }
}
