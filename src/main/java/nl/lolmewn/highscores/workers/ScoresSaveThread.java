/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores.workers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class ScoresSaveThread implements Runnable {

    private final Main plugin;

    public ScoresSaveThread(Main main) {
        this.plugin = main;
    }

    @Override
    public void run() {
        plugin.debug("Saving players...");
        Connection con = plugin.getStatsAPI().getConnection();
        try {
            con.setAutoCommit(false);
            PreparedStatement update = con.prepareStatement("UPDATE " + plugin.USER_TABLE + " SET value=?, isHighscore=? WHERE player_id=? AND highscoreId=?");
            PreparedStatement insert = con.prepareStatement("INSERT INTO " + plugin.USER_TABLE + " (player_id, value, highscoreId, isHighscore) VALUES (?, ?, ?, ?)");
            for (Highscore score : plugin.getHighscoreManager().getHighscores()) {
                Set<UUID> withUpdates = score.getUsersWithUpdate();
                synchronized (withUpdates) {
                    Iterator<UUID> it = withUpdates.iterator();
                    while (it.hasNext()) {
                        UUID playerUUID = it.next();
                        int id = plugin.getStatsAPI().getPlayerId(playerUUID);
                        if (!score.hasListing(playerUUID)) {
                            it.remove();
                            continue; //dafuq?
                        }
                        update.setDouble(1, score.getListing(playerUUID, true));
                        update.setBoolean(2, score.getCurrentHighscorePlayerUUID().equals(playerUUID));
                        update.setInt(3, id);
                        update.setInt(4, score.getId());
                        update.addBatch();
                        plugin.debug("Updating: " + update.toString());
                    }
                    int[] res = update.executeBatch();
                    boolean needsInsert = false;
                    for (int i = 0; i < res.length; i++) {
                        int result = res[i];
                        UUID playerUUID = (UUID) withUpdates.toArray()[i];
                        int id = plugin.getStatsAPI().getPlayerId(playerUUID);
                        if (result == 0) {
                            needsInsert = true;
                            insert.setInt(1, id);
                            insert.setDouble(2, score.getListing(playerUUID, true));
                            insert.setInt(3, score.getId());
                            insert.setBoolean(4, score.getCurrentHighscorePlayerUUID().equals(playerUUID));
                            insert.addBatch();
                            plugin.debug("Inserting: " + insert.toString());
                        }
                    }
                    if (needsInsert) {
                        insert.executeBatch();
                    }
                    withUpdates.clear();
                }
            }
            con.commit();
            update.close();
            insert.close();
        } catch (SQLException ex) {
            Logger.getLogger(ScoresSaveThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ScoresSaveThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
