package nl.lolmewn.highscores.workers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.UUID;
import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.highscores.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class ListingsSender implements Runnable {

    private final CommandSender sender;
    private final UUID lookup;
    private final int page;
    private final Main plugin;

    public ListingsSender(CommandSender sender, UUID lookup, int page, Main plugin) {
        this.sender = sender;
        this.lookup = lookup;
        this.page = page;
        this.plugin = plugin;
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, this);
    }

    @Override
    public void run() {
        int pp = plugin.getSettings().getItemsPerPage();
        int shown = 0;
        int skipped = 0;
        final List<String> send = new ArrayList<String>();
        for (Highscore score : plugin.getHighscoreManager().getHighscores()) {
            if (skipped < pp * (page - 1)) {
                skipped++;
                continue;
            }
            if (shown == pp) {
                break;
            }
            int i = 1;
            SortedSet<Map.Entry<UUID, Double>> set = score.entriesSortedByValues();
            Iterator<Map.Entry<UUID, Double>> it = set.iterator();
            while (it.hasNext()) {
                Map.Entry<UUID, Double> entry = it.next();
                if (!entry.getKey().equals(lookup)) {
                    i++;
                    continue;
                }
                send.add(ChatColor.GREEN + "#" + ChatColor.RED + i + ChatColor.GREEN + " in highscore " + score.getName() + " (" + entry.getValue() + ")");
                break;
            }
        }
        plugin.getServer().getScheduler().runTask(plugin, new Runnable() {

            @Override
            public void run() {
                for(String message : send){
                    sender.sendMessage(message);
                }
            }
        });
    }

}
