/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.highscores;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import nl.lolmewn.highscores.completion.Completion;
import nl.lolmewn.highscores.completion.CompletionType;
import nl.lolmewn.highscores.goal.Goal;
import nl.lolmewn.highscores.reward.Reward;
import nl.lolmewn.highscores.reward.RewardType;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.mysql.MySQLType;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class HighscoreManager {

    private final HashMap<Stat, List<Highscore>> highscores = new HashMap<Stat, List<Highscore>>();
    private final HashMap<Integer, Highscore> highscoreIdBinder = new HashMap<Integer, Highscore>();
    private final Main main;
    private final int DEFAULT_OFFSET = 9001;

    public HighscoreManager(Main main) {
        this.main = main;
    }

    public void loadHighscores() throws SQLException {
        File file = new File(main.getDataFolder(), "highscores.yml");
        if (!file.exists()) {
            main.saveResource("highscores.yml", true);
        }
        Connection con = this.main.getStatsAPI().getConnection();
        YamlConfiguration c = YamlConfiguration.loadConfiguration(file);
        for (String key : c.getConfigurationSection("").getKeys(false)) {
            try {
                int id = Integer.parseInt(key);
                if (this.highscoreIdBinder.containsKey(id)) {
                    main.getLogger().warning("Duplicate highscore ID found, this is now allowed. Only the first highscore will be loaded with ID " + id);
                    continue;
                }
                Highscore score = new Highscore(main, id, c.getConfigurationSection(key));
                for (Goal g : score.getGoals()) {
                    this.addHighscore(g.getType(), score);
                }
            } catch (NumberFormatException e) {
                main.getLogger().warning("Couldn't load highscore with ID '" + key + "', must be a number.");
            }
        }
        if (main.getSettings().isAllStatsDefaultHighscores()) {
            for (final Stat stat : main.getStatsAPI().getAllStats()) {
                if (stat.getMySQLType().equals(MySQLType.TIMESTAMP)) {
                    continue; //it's silly to have that.
                }
                if (!main.getStatsAPI().isStatEnabled(stat.getName())) {
                    continue;
                }
                List<Goal> goals = new ArrayList<Goal>() {
                    {
                        this.add(new Goal(stat, true, new Object[]{}));
                    }
                    private static final long serialVersionUID = 1L;
                };
                List<Reward> rewards = new ArrayList<Reward>() {
                    {
                        for (ItemStack stack : main.getSettings().getDefaultItemRewards()) {
                            this.add(new Reward(RewardType.ITEM, stack));
                        }
                    }
                    private static final long serialVersionUID = 1L;
                };
                List<Completion> comp = new ArrayList<Completion>() {
                    {
                        this.add(new Completion(CompletionType.MESSAGE, "&cYou now hold the highscore for &b%name%"));
                        this.add(new Completion(CompletionType.BROADCAST, "&6%player% &cnow holds the new highscore for &b%name%!"));
                    }
                    private static final long serialVersionUID = 1L;
                };
                int id = main.getMysqlHelper().getAchievementID(con, stat, main.HIGHSCORE_TABLE);
                if(id == -1){
                    id = main.getMysqlHelper().getNextFreeID(con, main.HIGHSCORE_TABLE) + this.DEFAULT_OFFSET + this.highscoreIdBinder.size();
                }
                Highscore score = new Highscore(main, id,
                        stat.getName(),
                        "The highscore for " + stat.getName(),
                        goals, rewards, comp, stat);
                this.addHighscore(stat, score);
            }
        }
        con.close();
    }

    public void loadScoresForHighscores() throws SQLException {
        Connection con = main.getStatsAPI().getConnection();
        PreparedStatement st = con.prepareStatement("SELECT * FROM " + main.USER_TABLE + " WHERE highscoreId=?");
        for (Highscore high : this.getHighscores()) {
            st.setInt(1, high.getId());
            ResultSet set = st.executeQuery();
            if (set == null) {
                main.getLogger().warning("ResultSet was null, something was probably wrong with the query.");
                continue;
            }
            while (set.next()) {
                int playerId = set.getInt("player_id");
                OfflinePlayer op = main.getServer().getOfflinePlayer(main.getStatsAPI().getPlayerName(playerId));
                long value = set.getLong("value");
                high.addListing(op.getUniqueId(), value, false);
                high.setDbListing(op.getUniqueId(), value);
                if (set.getBoolean("isHighscore")) {
                    high.setCurrentHighscorePlayer(op.getUniqueId());
                    high.setCurrentHighscoreValue(value);
                }
            }
            set.close();
        }
        st.close();
        con.close();
    }

    public void addHighscore(Stat type, final Highscore score) {
        this.highscoreIdBinder.put(score.getId(), score);
        if (this.highscores.containsKey(type)) {
            this.highscores.get(type).add(score);
        } else {
            this.highscores.put(type, new ArrayList<Highscore>() {
                {
                    this.add(score);
                }
                private static final long serialVersionUID = 1L;
            });
        }
    }

    public List<Highscore> getHighscoresForStat(Stat type) {
        return this.highscores.get(type);
    }

    public Set<Stat> getMapKeys() {
        return this.highscores.keySet();
    }

    public String getPlayerWithHighscore(int highscoreId) {
        return this.highscoreIdBinder.get(highscoreId).getCurrentHighscorePlayer();
    }

    public Collection<Highscore> getHighscores() {
        return this.highscoreIdBinder.values();
    }

    public Highscore getHighscore(int id) {
        return this.highscoreIdBinder.get(id);
    }
}
