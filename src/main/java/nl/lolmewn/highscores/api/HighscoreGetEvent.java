package nl.lolmewn.highscores.api;

import nl.lolmewn.highscores.Highscore;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Lolmewn
 */
public class HighscoreGetEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final StatsPlayer player;
    private final Highscore highscore;
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public HighscoreGetEvent(StatsPlayer player, Highscore highscore){
        this.player = player;
        this.highscore = highscore;
    }

    public StatsPlayer getPlayer() {
        return player;
    }

    public Highscore getHighscore() {
        return highscore;
    }
    
    public double getNewHighscoreValue() {
        return this.highscore.getCurrentHighscoreValue();
    }

}
