/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.highscores.completion;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public enum CompletionType {

    MESSAGE, BROADCAST;

}
